
import { API } from "./config";

export const anyFetch = ({
    uri = '',
    body = false,
    method = "GET"
}) => {
    return fetch(`${API}${uri}`, {
        method: method,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        body: body ? JSON.stringify(body) : undefined
    })
        .then((response) => {
            return response.json();
        })
        .catch((err) => {
            console.log(err);
        });
};


