import { anyFetch } from "./fetch_functions";


export const getCategories = () => {
    return anyFetch({
      uri:`category/`,
      method:'GET'
    })
};

export const getSubCategories = (categoryId) => {
    return anyFetch({
      uri:`subcategory/byCategory/${categoryId}`,
      method:'GET'
    })
};

export const getSubCategoryTpes= (subcategoryId) => {
    return anyFetch({
      uri:`subcategorytype/bySubCategory/${subcategoryId}`,
      method:'GET'
    })
};

export const getProducts= (subcategoryTypeId) => {
    return anyFetch({
      uri:`subcategorytype/${subcategoryTypeId}`,
      method:'GET'
    })
};