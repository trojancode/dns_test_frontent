
import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Home from './Routes/Home'
import Products from './Routes/Products'
import SubCategory from './Routes/SubCategory'
import SubCategoryTypes from './Routes/SubCategoryTypes'
const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Home}></Route>
                <Route path="/subcategory/:categoryId" exact component={SubCategory}></Route>
                <Route path="/subcategory/subtypes/:subcategoryId" exact component={SubCategoryTypes}></Route>
                <Route path="/products/:subcategoryTypeId" exact component={Products}></Route>
            </Switch>
        </BrowserRouter>
    )
}

export default Routes
