import React, { useEffect, useState } from 'react'
import { Div, Row, Icon, Col, Image, Text, Input, Container } from "atomize";
import Layout from '../components/Layout';
import {  getSubCategoryTpes } from '../core/categoryApis';
import SubCategoryTypeItem from '../components/SubCategoryTypeItem';
import { useParams } from 'react-router-dom'
import ProductList from '../components/ProductList';

const SubCategoryTypes = (props) => {
    const { subcategoryId } = useParams()
    const [data, setData] = useState({
        data: {},
        isLoading: false
    })

    const fetchSubCategoriesTypes=()=>{
        setData({...data,isLoading:true})
        getSubCategoryTpes(subcategoryId).then(data =>{
            if(!data?.error){
                setData({...data,data:data,isLoading:false})
            }else{
                setData({...data,isLoading:false})
            }
        })
    }

    useEffect(() => {
        fetchSubCategoriesTypes()
    }, [])


    return (
        <Layout isLoading={data.isLoading}>
            <Text m={{y:"25px"}} textSize="heading">{data.data?.subcategory?.name} ({data.data?.subcategory?.products.length})</Text>
            <Container>
                    {
                        data.data?.subcategoryTypes?.map((item,i)=>(
                            <SubCategoryTypeItem data={item} key={i}></SubCategoryTypeItem>
                        ))
                    }
            </Container>
            <ProductList data={data.data?.subcategory?.products}></ProductList>
            
        </Layout>
    )
}

export default SubCategoryTypes
