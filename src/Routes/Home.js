import React, { useEffect, useState } from 'react'
import { Div, Row, Icon, Col, Image, Text, Input, Container } from "atomize";
import Layout from '../components/Layout';
import { getCategories } from '../core/categoryApis';
import Categoryitem from '../components/Categoryitem';
import ProductList from '../components/ProductList';

const Home = () => {
    const [data, setData] = useState({
        data: [],
        isLoading: false
    })

    const fetchCategories = () => {
        setData({ ...data, isLoading: true })
        getCategories().then(data => {
            if (!data?.error) {
                setData({ ...data, data: data, isLoading: false })
            } else {
                setData({ ...data, isLoading: false })
            }
        })
    }

    useEffect(() => {
        fetchCategories()
    }, [])

    // const getProductsCount=()=>{
    //     return data.data.reduce((count, current) => count + current.products.length, 0);
    // }

    const GetAllProduct = (data) => {
        let products = [];
        data.map((item, i) => {
            products = products.concat(item.products)
        });
        return products;
    }

    return (
        <Layout isLoading={data.isLoading}>
            <Text m={{ y: "25px" }} textSize="heading">Categories ({GetAllProduct(data.data)?.length})</Text>
            <Container>
                {
                    data.data?.map((item, i) => (
                        <Categoryitem data={item} key={i}></Categoryitem>
                    ))
                }
            </Container>
            <ProductList data={GetAllProduct(data.data)}></ProductList>
        </Layout>
    )
}

export default Home
