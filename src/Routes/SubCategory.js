import React, { useEffect, useState } from 'react'
import { Div, Row, Icon, Col, Image, Text, Input, Container } from "atomize";
import Layout from '../components/Layout';
import { getSubCategories } from '../core/categoryApis';
import SubCategoryItem from '../components/SubCategoryItem';
import ProductList from '../components/ProductList';
import { useParams } from 'react-router-dom'

const SubCategory = (props) => {
    const { categoryId } = useParams()
    const [data, setData] = useState({
        data: {},
        isLoading: false
    })

    const fetchSubCategories=()=>{
        setData({...data,isLoading:true})
        getSubCategories(categoryId).then(data =>{
            if(!data?.error){
                setData({...data,data:data,isLoading:false})
            }else{
                setData({...data,isLoading:false})
            }
        })
    }

    useEffect(() => {
        fetchSubCategories()
    }, [])


    return (
        <Layout isLoading={data.isLoading}>
            <Text m={{y:"25px"}} textSize="heading">{data.data?.category?.name} ({data.data?.category?.products.length})</Text>
            <Container>
                    {
                        data.data?.subcategories?.map((item,i)=>(
                            <SubCategoryItem data={item} key={i}></SubCategoryItem>
                        ))
                    }
            </Container>
            <ProductList data={data.data?.category?.products}></ProductList>
        </Layout>
    )
}

export default SubCategory
