import React, { useEffect, useState } from 'react'
import { Div, Row, Icon, Col, Image, Text, Input, Container } from "atomize";
import Layout from '../components/Layout';
import {  getProducts, getSubCategoryTpes } from '../core/categoryApis';
import ProductItem from '../components/ProductItem';
import { useParams } from 'react-router-dom'

const Products = (props) => {
    const { subcategoryTypeId } = useParams()
    const [data, setData] = useState({
        data: {},
        isLoading: false
    })

    const fetchProducts=()=>{
        setData({...data,isLoading:true})
        getProducts(subcategoryTypeId).then(data =>{
            if(!data?.error){
                setData({...data,data:data,isLoading:false})
            }else{
                setData({...data,isLoading:false})
            }
        })
    }

    useEffect(() => {
        fetchProducts()
    }, [])


    return (
        <Layout isLoading={data.isLoading}>
            <Text m={{y:"25px"}} textSize="heading">{data.data?.name}</Text>
            <Container>
                    {
                        data.data?.products?.map((item,i)=>(
                            <ProductItem data={item} key={i}></ProductItem>
                        ))
                    }
            </Container>
        </Layout>
    )
}

export default Products
