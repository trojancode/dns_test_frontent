import React from 'react'
import { Div, Row, Text } from 'atomize'
import { Link } from 'react-router-dom'

const ProductItem = ({ data }) => {
    return (
            <Div bg="white" m="3px" p={{ x: "10px", y: "8px" }} rounded="lg">
                <Row m="0" justify="space-between">
                    <Text textSize="subheader">{data.name}</Text>
                    <Text >{data.price}</Text>
                </Row>
            </Div>
    )
}

export default ProductItem
