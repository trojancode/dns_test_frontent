import React from 'react'
import { Container, Div } from 'atomize'

const ProductList = ({data}) => {
    return (
        <Container>
            <Div m={{ t: "25px" }} bg="white" rounded="lg">
                <table>
                    <thead>
                        <tr>
                            <th>Product name</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data?.map((item, i) => (
                                <tr>
                                    <td>{item.name}</td>
                                    <td>${item.price}</td>
                                </tr>
                            ))
                        }

                    </tbody>
                </table>
            </Div>
        </Container>
    )
}

export default ProductList
