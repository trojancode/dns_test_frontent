import React from 'react'
import { Div, Row, Text } from 'atomize'
import { Link } from 'react-router-dom'

const SubCategoryItem = ({ data }) => {
    return (
        <Link to={`/subcategory/subtypes/${data._id}`}>
            <Div bg="white" m="3px" p={{ x: "10px", y: "8px" }} rounded="lg">
                <Row m="0" justify="space-between">
                    <Text textSize="subheader">{data.name}</Text>
                    <Text >{data.products.length}</Text>
                </Row>
            </Div>
        </Link>
    )
}

export default SubCategoryItem
