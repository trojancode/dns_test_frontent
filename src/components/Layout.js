import React from 'react'
import { Div,Image,Icon } from 'atomize'

const Layout = ({isLoading=false,children}) => {
    return (
        <Div h="100vh">
            <Div bg="lightblue" p={{ x: "20px", y: "10px" }}>
                <Image src="/logo.png" w="130px"></Image>
            </Div>
            <Div bg="mainBg" minH="calc(100% - 64px)" w="100%" textAlign="center" p={{ y: "15px", x: "15px" }}>
                { isLoading?(
                    <Div minH="300px" d="flex" align="center" justify="center" minW="100%">
                            <Icon name="Loading2" size="40px"></Icon>
                    </Div>
                ):children}
            </Div>

        </Div>
    )
}

export default Layout
